//film 2 block scope 

// var Name='caniel'

// if(true){
//     console.log(Name)
// }
// => caniel

'--------------------------'
// if(true){
//     var name='stefan'
// }
// console.log(name) // => stefan

'--------------------------'
// let age=19

// if (true) {
//     console.log(age)
// } // => 19

'--------------------------'
// if (true) {
//     let age=19
// }
// console.log(age) // Error: age is not defined

'------------------------------------------------------------------------------------------------------------------------------------'
//film 3 let const
// age=19
// let age
// console.log(age) // Error

'-----------------------------'
// let name
// name='caniel'
// console.log(name) // =>camiel

'-----------------------------'
// let color="red"
// color='yellow'
// console.log(color) // =>yellow

'-----------------------------'

// const product='book'
// product='pen'
// console.log(product) // => Error: Assignment to constant variable

'--------------------------------------------------------------------------------------------------------------------------------------'
//film 4 Arrow function

// function user() {
//     console.log('welcome!')
// }
// user() 

'----------------------------'
// const user = () => {
//     console.log('welcome')
// }
// user()

'----------------------------'
// const user = (userName, password) => {
//     console.log(userName)
//     console.log(password)
// }
// user('yenson', 5697262)

'----------------------------------------------------------------------------------------------------------------------------------------'
//film 5 default

// const sum = (a = 5, b = 2) => {
//     console.log(a + b)
// }
// sum() //=> 5+2=7
// sum(3, 8) //=> 3+8=11

'------------------------------------------------------------------------------------------------------------------------------------------'
//film 6

// let age = 74
// const obj = {
//     name: 'caniel',
//     age
// }

// console.log(obj.age)

// let emailAddres = 'email'
// const user = {
//     id: 45,
//     [emailAddres]: 'caielotiis56@gmail.com'
// }
// console.log(user['email'])

// console.log(user['id'])

'-----------------------------------------------------------------------------------------------------------------------------------------'
//film 7 for of

// const students = ["caniel", "yenson", "otiis"]

// for (let item of students) {
//     console.log(item)
// }   //=> caniel 
// yenson
// otiis

'------------------------------------------------------------------------------------------------------------------------------------------'
//film 8 template string

// let name='
// caniel'
// console.log(name) //=> Error 

'----------------------------'
// let name=`
// stefan`
// console.log(name)  //=> "
//   stefan"

'-----------------------------'
// let name = 'caniel'
// let user = `${name} welcome!!`
// console.log(user) //=> caniel welcome!!

'-----------------------------------------------------------------------------------------------------------------------------------------'
//film 9 array destructuring

// let students = ["caniel", "yenson", "otiis"]

// let [a, b, c] = students
// console.log(a, b, c) //=> caniel yenson otiis

'---------------------------------------------'
// let students = ["caniel", "yenson", "otiis"]

// let [a, b, c, d = "stefan"] = students
// console.log(a, b, c, d) //=>caniel yenson otiis stefan

'--------------------------------------------------------------------------------------------------------------------------------------------'
//film 10 Object destructuring

// let user={
//     userName:"yenson",
//     id:45
// }
// let {userName,id}=user
// console.log(userName,id) //=> yenson 45

'---------------------------------'
// let user={
//     userName:"yenson",
//     id:45
// }
// let {userName,id,password=96842738}=user
// console.log(userName,id,password) //=> yenson 45 96842738

'-------------------------------------------------------------------------------------------------------------------------------------------'
//film 11 export import

// let sum = (a, b) => {
//     let z = a + b
//     return z
// }

// export { sum }

'-------------------------------'
// export let name = 'caniel'

// let age = 45

// let students = ["caniel", "yenson", "otiis"]

// let user = {
//     userName: "yenson",
//     id: 45
// }

'---------------------------------'
// export {
//     age as default,
//     students as stu,
//     user
// }

'---------------------------------'

// import {sum} from './java'

'---------------------------------------------------------------------------------------------------------------------------------------------'
//film 12 class

// class User {
//     constructor(id, userName) {
//         this.id = id
//         this.userName = userName
//     }
//     userInsfo() {
//         return `id user is: ${this.id} - user name is: ${this.userName}`
//     }
// }

// console.log(new User(13, 'yenson').userInsfo()) //=> id user is: 13 - user name is: yenson

'--------------------------------------------------------------------------------------------------------------------------------------------'
//film  13 calss

// class User {
//     constructor(id, userName) {
//         this.id = id
//         this.userName = userName
//     }
//     userInsfo() {
//         return `id user is: ${this.id} - user name is: ${this.userName} `
//     }
// }

// class userCreat extends User {
//     constructor(id, userName, email) {
//         super(id, userName)
//         this.email = email
//     }
// }

// console.log(new userCreat(13, "caniel", 'caielotiis56@gmail.com').userInsfo()) //=> id user is: 13 - user name is: caniel

'------------------------------------------------------------------------------------------------------------'
