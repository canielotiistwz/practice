//film 51 52 53 54 56 58

const titleElement = document.querySelector('#product-title')
const priceElement = document.querySelector('#product-price')
const removeElement = document.querySelector('#remove-product')
const dateElement = document.querySelector('#last-Edit')

const productId = location.hash.substring(1)
let products = getSaveProducts()
let product = products.find(item => item.id === productId)

if (product === undefined) {
    location.assign("../../html/sadri/app.html")
}

titleElement.value = product.title
priceElement.value = product.price
dateElement.textContent = lastEditMassege(product.updated)
'--------------------------------------------------------'
//53

// titleElement.addEventListener('change', function (e) {
//     e.preventDefault()
//     if (e.target.value !== products.title) {
//         products.push({
//             title: titleElement.value
//         })
//         e.target.value = titleElement.value
//         saveProducts(products)

//     }
// })

// removeElement.addEventListener('click', function () {
//     titleElement.value = ''
//     priceElement.value = ''
// })

'--------------------------------------------------------------'
titleElement.addEventListener('input', (e) => {
    product.title = e.target.value
    product.updated = moment().valueOf()
    dateElement.textContent = lastEditMassege(product.updated)
    saveProducts(products)
})

priceElement.addEventListener('input', (e) => {
    product.price = e.target.value
    product.updated = moment().valueOf()
    dateElement.textContent = lastEditMassege(product.updated)
    saveProducts(products)
})

removeElement.addEventListener('click', (e) => {
    removeProducts(product.id)
    saveProducts(products)
    location.assign("../../html/sadri/app.html")
})

window.addEventListener('storage', (e) => {
    if (e.key === 'products') {
        products = JSON.parse(e.newValue)
        saveProducts(products)
        product = products.find(item => item.id === productId)

        if (product == undefined) {
            location.assign("../../html/sadri/app.html")
        }
        titleElement.value = product.title
        priceElement.value = product.price
        dateElement.textContent = lastEditMassege(product.updated)
    }
})




