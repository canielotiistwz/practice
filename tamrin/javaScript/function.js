//film 46 47 50 58

let getSaveProducts = () => {
    const productJSON = localStorage.getItem('products')
    try {
        return productJSON !== null ? JSON.parse(productJSON) : []
    } catch (e) {
        return []
    }


    // if (productJSON !== null) {
    //     return JSON.parse(productJSON)
    // } else {
    //     return []
    // }
}
'------------------------------------------------------------------------'
const saveProducts = (products) => {
    localStorage.setItem('products', JSON.stringify(products))
}

'------------------------------------------------------------------------'
const removeProducts = (id) => {
    const productIndex = products.findIndex(item => item.id === id)
    if (productIndex > -1) {
        products.splice(productIndex, 1)
    }
}

'-------------------------------------------------------------------------'
const toggleProduct = (id) => {
    let productExsit = products.find(item => item.id === id)
    if (productExsit !== undefined) {
        productExsit.exist = !productExsit.exist
    }
}

'--------------------------------------------------------------------------'

const sortProducts = (products, sortBy) => {
    if (sortBy === 'byEdited') {
        return products.sort((a, b) => {
            if (a.updated > b.updated) {
                return -1
            } else if (a.updated < b.updated) {
                return 1
            } else {
                return 0
            }
        })
    } else if (sortBy === 'byCreated') {
        return products.sort((a, b) => {
            if (a.created > b.created) {
                return -1
            } else if (a.created < b.created) {
                return 1
            } else {
                return 0
            }
        })
    } else {
        return products
    }
}

'------------------------------------------------------------------------'
const renderProducts = (products, filters) => {
    products = sortProducts(products, filters.sortBy)
    let filteredProduct = products.filter((item) => {
        return item.title.toLowerCase().includes(filters.searchItem.toLowerCase())
    })

    filteredProduct = filteredProduct.filter((item) => {
        if (filters.avilabaleProducts) {
            return item.exist
        } else {
            return true
        }
    })

    document.querySelector('#search-product').innerHTML = ''
    filteredProduct.forEach((item) => {
        document.querySelector('#search-product').appendChild(creatProductDom(item))

    })

}

'------------------------------------------------------------------------'
// const creatProductDom = function (product) {
//     let renderSearch = document.createElement('p')
//     renderSearch.textContent = product.title
//     return renderSearch
// }

'------------------------------------------------------------------------'
const creatProductDom = (product) => {
    let produdtEl = document.createElement('div')
    let checkBox = document.createElement('input')
    let productItem = document.createElement('a')
    let productPrice = document.createElement('a')
    let removeButton = document.createElement('button')

    checkBox.setAttribute('type', 'checkbox')
    checkBox.checked = !product.exist
    produdtEl.appendChild(checkBox)
    checkBox.addEventListener('change', () => {
        toggleProduct(product.id)
        saveProducts(products)
        renderProducts(products, filters)
    })

    productItem.setAttribute('href', `../sadri/Edit-Product.html#${product.id}`)
    productItem.textContent = product.title
    produdtEl.appendChild(productItem)

    productPrice.setAttribute('href',`../sadri/Edit-Product.html#${product.id}`)
    productPrice.textContent = product.price
    produdtEl.appendChild(productPrice)

    removeButton.textContent = 'remove'
    produdtEl.appendChild(removeButton)
    removeButton.addEventListener('click', () => {
        removeProducts(product.id)
        saveProducts(products)
        renderProducts(products, filters)
    })

    return produdtEl

}

'-----------------------------------------------------'
//film 58
const lastEditMassege = (timeStamp) => {
    return `last Edit: ${moment(timeStamp).locale('fa').fromNow()}`
}

'----------------------------------------------------------------'
